// gowiki project wiki.go
package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"runtime"
)

type Page struct {
	Title string
	Body  []byte
}

var templates = template.Must(template.ParseGlob("templates/*.html"))

//var templates *template.Template
var validPath = regexp.MustCompile("^/(index|edit|save|view)/([a-zA-z0-9' '-.é&]+)$")

func (p *Page) save() error {
	//filename := p.Title + ".md"
	filename := p.Title
	return ioutil.WriteFile("data/"+filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	//filename := title + ".md"
	filename := title
	body, err := ioutil.ReadFile("data/" + filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

func readFileNames() []string {
	dir, err := os.Open("data")
	if err != nil {
		log.Fatalf("failed opening directory: %s", err)
	}
	defer dir.Close()
	list, _ := dir.Readdirnames(0) // 0 to read all file names
	return list
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	titles := readFileNames()
	err := templates.ExecuteTemplate(w, "index.html", titles)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func open(url string) error {
	var cmd string
	var args []string
	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))
	//log.Fatal(http.ListenAndServe(":5033", nil))

	l, err := net.Listen("tcp", "localhost:5033")
	if err != nil {
		log.Fatal(err)
	}
	err = open("http://localhost:5033/")
	if err != nil {
		log.Println(err)
	}
	http.Serve(l, nil)
}
